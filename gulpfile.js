'use strict';

var gulp = require('gulp'),  
	sass = require('gulp-sass'),  
	browserSync = require('browser-sync');

gulp.task('sass', function () {  
    gulp.src('css/styles.scss')
        .pipe(sass({includePaths: ['scss']}))
        .pipe(gulp.dest('css'));
});

gulp.task('browser-sync', function() {  
    browserSync.init(["./*.html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js"], {
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('default', ['sass', 'browser-sync'], function () {  
    gulp.watch("css/*.scss", ['sass']);
});